// // server.js (exemple avec Express)
// const express = require('express');
// const cors = require('cors');

// const app = express();

// app.use(cors());

// // Gère les requêtes OPTIONS pour toutes les routes
// app.options('*', cors());

// // ... autres configurations et routes de votre serveur ...

// const PORT = process.env.PORT || 3000;
// app.listen(PORT, () => {
//     console.log(`Server is running on port ${PORT}`);
// });
