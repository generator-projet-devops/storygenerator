import { NextResponse } from "next/server";

export function middleware() {
  // Retrieve the current response
  const res = NextResponse.next();

  // Add the CORS headers to the response
  res.headers.append("Access-Control-Allow-Origin", "*"); // Update this to the specific origin or origins you want to allow
  res.headers.append("Access-Control-Allow-Credentials", "true");
  res.headers.append(
    "Access-Control-Allow-Methods",
    "GET,DELETE,PATCH,POST,PUT,OPTIONS"
  );
  res.headers.append(
    "Access-Control-Allow-Headers",
    "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
  );

  // Check if the request method is OPTIONS and handle it
  if (res.method === "OPTIONS") {
    res.status = 200;
    res.end();
    return res;
  }

  // Continue processing other requests

  return res;
}

// Specify the path regex to apply the middleware to
export const config = {
  matcher: "/api/:path*",
};
