import Head from "next/head";
import { useState } from "react";
import styles from "./index.module.css";
import Loading from "react-loading";

export default function Home() {
  const [topicInput, setTopicInput] = useState("");
  const [result, setResult] = useState("");
  const [loading, setLoading] = useState(false);
  const [language, setLanguage] = useState("en"); // State for language selection

  async function onSubmit(event) {
    event.preventDefault();
    setLoading(true);
    try {
      const response = await fetch("/api/generate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ topic: topicInput, language }),
      });

      const data = await response.json();
      if (response.status !== 200) {
        throw (
          data.error ||
          new Error(`Request failed with status ${response.status}`)
        );
      }

      setResult(data.result);
      setTopicInput("");
    } catch (error) {
      console.error(error);
      alert(error.message);
    } finally {
      setLoading(false);
    }
  }

  return (
    <div>
      <Head>
        <title>OpenAI Quickstart</title>
        <link rel="icon" href="/dog.png" />
      </Head>

      <main className={styles.main}>
        <img src="/dog.png" className={styles.icon} />
        <h3>Create a Story</h3>
        <form onSubmit={onSubmit}>
          <input
            type="text"
            name="topic"
            placeholder="Enter a topic for your story"
            value={topicInput}
            onChange={(e) => setTopicInput(e.target.value)}
          />
          <select
            value={language}
            onChange={(e) => setLanguage(e.target.value)}
          >
            <option value="en">English</option>
            <option value="fr">French</option>
          </select>
          <input type="submit" value="Generate story" />
        </form>
        {loading ? (
          <div className={styles.loading}>
            <Loading type="bars" color="#000" height={64} width={64} />
            <p>Loading...</p>
          </div>
        ) : (
          <div className={styles.result}>{result}</div>
        )}
      </main>
    </div>
  );
}
