import { Configuration, OpenAIApi } from "openai";
import NextCors from "nextjs-cors";

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function handler(req, res) {
  // Enable CORS using NextCors
  await NextCors(req, res, {
    methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
    origin: "*",
    optionsSuccessStatus: 200,
  });

  if (!configuration.apiKey) {
    res.status(500).json({
      error: {
        message:
          "OpenAI API key not configured, please follow instructions in README.md",
      },
    });
    return;
  }

  const topic = req.body.topic || "";
  const language = req.body.language || "en";

  if (topic.trim().length === 0) {
    res.status(400).json({
      error: {
        message: "Please enter a valid topic",
      },
    });
    return;
  }

  try {
    const completion = await openai.createCompletion({
      model: language === "fr" ? "text-davinci-003" : "text-davinci-002",
      prompt: generatePrompt(topic, language),
      temperature: 0.6,
      max_tokens: 400,
    });
    res.status(200).json({ result: completion.data.choices[0].text });
  } catch (error) {
    res.status(500).json({
      error: {
        message: "An error occurred while generating the story.",
      },
    });
  }
}

function generatePrompt(topic, language) {
  const languagePrompt = language === "fr" ? "en français" : "in English";
  return `Write an  story about ${topic}. 
  This story should be written  ${languagePrompt}.`;
}
