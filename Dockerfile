# Utiliser une image de base Node.js
FROM node:latest

# Définir le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copier les fichiers de votre application dans le conteneur
COPY . .

# Installer les dépendances
RUN npm install

# Exposer le port utilisé par votre application (par exemple, 3000 pour Next.js)
EXPOSE 3000

# Set environment variable during container runtime


# Commande pour démarrer l'application
CMD ["npm", "run", "dev"]
